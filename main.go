package main

import (
	"booking-app/helper"
	"fmt"
)

const conferenceTickets int = 50

var conferenceName = "Go Conference"
var remainingTickets uint = 50

//var bookings = make([]map[string]string, 0)

// the type keyword creates a new type with the name you specify
type UserData struct {
	firstName       string
	lastName        string
	email           string
	numberOfTickets uint
}

// replacing the list of maps with a list of type UserData
var bookings = make([]UserData, 0)

func main() {

	greetUsers()

	for {
		firstName, lastName, email, userTickets := getUserInput()
		isValidName, isValidEmail, isValidTicketNumber := helper.ValidateUserInput(firstName, lastName, email, userTickets, remainingTickets)

		if isValidName && isValidEmail && isValidTicketNumber {
			// all input valid, start booking process
			bookTicket(userTickets, firstName, lastName, email)

			// calling a function to get a return value
			var firstNames []string = getFirstNames()
			fmt.Printf("The first names of bookings are %v\n", firstNames)

			if remainingTickets == 0 {
				// end program
				fmt.Println("Our conference is book out. Please come back next year.")
				break // out of the for loop
			}
		} else {
			// fmt.Printf("We only have %v tickets remaining, so you can't book %v tickets\n", remainingTickets, userTickets)

			// adding individual statements so each message is shown
			// about what is wrong
			if !isValidName {
				fmt.Println("First name or lastname you entered is too short.")
			}
			if !isValidEmail {
				fmt.Println("Email address you enterd doesn't contain @ sign.")
			}
			if !isValidTicketNumber {
				fmt.Println("Number of tickets you entered is invalid.")
			}
		}
	}
}

func greetUsers() {
	fmt.Printf("Welcome to %v booking application.\n", conferenceName)
	fmt.Printf("We have total of %v tickets and %v are still available\n", conferenceTickets, remainingTickets)
	fmt.Println("Get your tickets here to attend")
}

func getFirstNames() []string {
	firstNames := []string{} // starting with an empty slice of type string
	// we use the underscore (blank identifier) for the index, as we dont use it
	for _, booking := range bookings {
		//var names = strings.Fields(booking)
		//firstNames = append(firstNames, booking["firstname"])
		firstNames = append(firstNames, booking.firstName)

	}
	return firstNames
}

func getUserInput() (string, string, string, uint) {
	var firstName string
	var lastName string
	var email string
	var userTickets uint

	// ask user for their name
	fmt.Println("Enter your first name ")
	fmt.Scan(&firstName)
	fmt.Println("Enter your last name ")
	fmt.Scan(&lastName)
	fmt.Println("Enter your email address ")
	fmt.Scan(&email)
	fmt.Println("Enter number of tickets ")
	fmt.Scan(&userTickets)

	return firstName, lastName, email, userTickets
}

func bookTicket(userTickets uint, firstName string, lastName string, email string) {
	// calculate remaining tickets
	remainingTickets = remainingTickets - userTickets

	// var userData = make(map[string]string)
	// userData["firstName"] = firstName
	// userData["lastName"] = lastName
	// userData["email"] = email
	// userData["numberOfTickets"] = strconv.FormatUint(uint64(userTickets), 10)
	var userData = UserData{
		firstName:       firstName,
		lastName:        lastName,
		email:           email,
		numberOfTickets: userTickets,
	}

	// bookings[0] = firstName + " " + lastName // this is for arrays
	bookings = append(bookings, userData)
	fmt.Printf("List of bookings is %v\n", bookings)

	// fmt.Printf("The whole array: %v\n", bookings)
	// fmt.Printf("The first value %v\n", bookings[0])
	// fmt.Printf("Array type %T\n", bookings)
	// fmt.Printf("Array lenght %v\n", len(bookings))

	fmt.Printf("Thank you %v %v for booking %v tickets. You will receive a confirmation email at %v\n", firstName, lastName, userTickets, email)
	fmt.Printf("%v tickets remaining for %v\n", remainingTickets, conferenceName)

}
